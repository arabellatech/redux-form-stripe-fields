"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var stripeValidator = function stripeValidator(value) {
  if (value && value.error) {
    return value.error.message;
  }

  return undefined;
};

exports.default = stripeValidator;