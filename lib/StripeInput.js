'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactStripeElements = require('react-stripe-elements');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StripeInput = function StripeInput(props) {
  var onChange = props.onChange,
      onBlur = props.onBlur,
      stripeElementOptions = props.stripeElementOptions,
      renderField = props.renderField,
      type = props.type;


  var elementProps = _extends({
    onChange: onChange,
    onBlur: onBlur
  }, stripeElementOptions);

  if (renderField) {
    return renderField(props);
  }

  switch (type) {
    case 'cardFull':
      return _react2.default.createElement(_reactStripeElements.CardElement, elementProps);
    case 'cardNumber':
      return _react2.default.createElement(_reactStripeElements.CardNumberElement, elementProps);
    case 'cardExpiry':
      return _react2.default.createElement(_reactStripeElements.CardExpiryElement, elementProps);
    case 'cardCVC':
      return _react2.default.createElement(_reactStripeElements.CardCVCElement, elementProps);
    case 'cardPostalCode':
      return _react2.default.createElement(_reactStripeElements.PostalCodeElement, elementProps);
    default:
      throw Error('Wrong stripe field type, must be one of "cardFull", "cardNumber", "cardExpiry", "cardCVC", "cardPostalCode"');
  }
};

StripeInput.propTypes = {
  onChange: _propTypes2.default.func.isRequired,
  onBlur: _propTypes2.default.func.isRequired,
  stripeElementOptions: _propTypes2.default.object,
  renderField: _propTypes2.default.func,
  type: _propTypes2.default.string
};

StripeInput.defaultProps = {
  stripeElementOptions: {},
  renderField: undefined,
  type: 'cardFull'
};

exports.default = StripeInput;