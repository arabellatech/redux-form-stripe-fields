'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _immutable = require('redux-form/immutable');

var _createFormField = require('react-form-fields/lib/createFormField');

var _createFormField2 = _interopRequireDefault(_createFormField);

var _createReduxFormField = require('react-form-fields/lib/createReduxFormField');

var _createReduxFormField2 = _interopRequireDefault(_createReduxFormField);

var _StripeInput = require('./StripeInput');

var _StripeInput2 = _interopRequireDefault(_StripeInput);

var _stripeValidator = require('./stripeValidator');

var _stripeValidator2 = _interopRequireDefault(_stripeValidator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CardInput = (0, _createReduxFormField2.default)((0, _createFormField2.default)(_StripeInput2.default));

var StripeField = function StripeField(props) {
  return _react2.default.createElement(_immutable.Field, _extends({
    component: CardInput,
    validate: _stripeValidator2.default
  }, props));
};

exports.default = StripeField;