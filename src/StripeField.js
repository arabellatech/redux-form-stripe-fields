import React from 'react';
import { Field } from 'redux-form/immutable';
import createFormField from 'react-form-fields/lib/createFormField';
import createReduxFormField from 'react-form-fields/lib/createReduxFormField';
import StripeInput from './StripeInput';
import stripeValidator from './stripeValidator';

const CardInput = createReduxFormField(createFormField(StripeInput));

const StripeField = (props) => (
  <Field
    component={CardInput}
    validate={stripeValidator}
    {...props}
  />
);

export default StripeField;
