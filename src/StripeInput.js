import React from 'react';
import PropTypes from 'prop-types';
import {
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  PostalCodeElement,
} from 'react-stripe-elements';

const StripeInput = (props) => {
  const {
    onChange,
    onBlur,
    stripeElementOptions,
    renderField,
    type,
  } = props;

  const elementProps = {
    onChange,
    onBlur,
    ...stripeElementOptions,
  };

  if (renderField) {
    return renderField(props);
  }

  switch (type) {
    case 'cardFull':
      return <CardElement {...elementProps} />;
    case 'cardNumber':
      return <CardNumberElement {...elementProps} />;
    case 'cardExpiry':
      return <CardExpiryElement {...elementProps} />;
    case 'cardCVC':
      return <CardCVCElement {...elementProps} />;
    case 'cardPostalCode':
      return <PostalCodeElement {...elementProps} />;
    default:
      throw Error('Wrong stripe field type, must be one of "cardFull", "cardNumber", "cardExpiry", "cardCVC", "cardPostalCode"');
  }
};

StripeInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  stripeElementOptions: PropTypes.object,
  renderField: PropTypes.func,
  type: PropTypes.string,
};

StripeInput.defaultProps = {
  stripeElementOptions: {},
  renderField: undefined,
  type: 'cardFull',
};

export default StripeInput;
