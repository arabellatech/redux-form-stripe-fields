const stripeValidator = (value) => {
  if (value && value.error) {
    return value.error.message;
  }

  return undefined;
};

export default stripeValidator;
